# TestNetwork

To accomplish network persistence it was decided to connect database;

To avoid spring data jpa tricks and implement unique nodes' names you have to manually execute the following command:

ALTER table nodes ADD CONSTRAINT nodes_name_unq UNIQUE (name);

Also you need to set up your database credentials in application.properties file.

Executable jar file located in the root directory of the project.

REST API explanation where in's necessary(Mostly you can easily understand what happens by reading the method's signature):

http://localhost:8080/manager/network/create

JSON example:

{
    "name": "FirstNetwork",
    "description": "First one",
	"amountOfSubStations": "1",
	"amountOfTransformers": "1",
	"amountOfFeeders": "1",
    "amountOfResources": "1"
    //params
}

http://localhost:8080/manager/node/update/{nodeId}

{
    "name": "FirstNode",
    "description": "First one"
    //params
}

http://localhost:8080/manager/network?type=SUBSTATION&name=NetworkExample

http://localhost:8080/manager/network/addChild?type=TRANSFORMER&name=NodeExample
{
    "name": "FirstNode",
    "description": "First one"
    //params
}

 Application was tested on REST API client.
----------------------------------------------------------------------------------------------

This work is unstable for now, have some bugs, but I hope it's enough for demonstrating my skills.

If you have any further questions, feel free to contact me :)



