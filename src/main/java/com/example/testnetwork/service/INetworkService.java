package com.example.testnetwork.service;

import com.example.testnetwork.enums.NodeType;
import com.example.testnetwork.model.Network;
import com.example.testnetwork.model.Node;

import java.util.List;
import java.util.Map;

public interface INetworkService {

     void createNetwork(List<Integer> sequence, String name, String description);

     Network findNetwork(long id);

     Network findNetwork(String name);

     void deleteNetwork(String name);

     void updateNetwork(Long id, List<Integer> sequence, String name, String description);

     void updateNode(Long id, String name, String description, Map<String, String> params);

     Network findNetworkByChildrenTypeAndName(NodeType nodeType, String name);

     Node findNodeEntityByTypeAndName(NodeType nodeType, String name);

     void addChildToSpecificNode(NodeType specificNodeType, String specificNodeName, String newNodeName, String newNodeDescription, Map<String, String> newNodeParams);

     void deleteSpecificNodeChild(NodeType nodeType, String nodeName, String target);
}


