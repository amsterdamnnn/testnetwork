package com.example.testnetwork.service.impl;

import com.example.testnetwork.enums.NodeType;
import com.example.testnetwork.exceptions.CreationNodeException;
import com.example.testnetwork.exceptions.NetworkDoesntExistException;
import com.example.testnetwork.exceptions.NodeDoesntExistException;
import com.example.testnetwork.mappers.Mapper;
import com.example.testnetwork.model.Network;
import com.example.testnetwork.model.Node;
import com.example.testnetwork.persistence.entity.ChildEntity;
import com.example.testnetwork.persistence.entity.NetworkEntity;
import com.example.testnetwork.persistence.entity.NodeEntity;
import com.example.testnetwork.persistence.repository.ChildRepository;
import com.example.testnetwork.persistence.repository.NetworkRepository;
import com.example.testnetwork.persistence.repository.NodeRepository;
import com.example.testnetwork.service.INetworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class NetworkService implements INetworkService {

    private static final Logger logger = LoggerFactory.getLogger(NetworkService.class);

    private NetworkRepository networkRepository;
    private ChildRepository childRepository;
    private NodeRepository nodeRepository;
    private Mapper<Network, NetworkEntity> networkModelToEntityMapper;
    private Mapper<NetworkEntity, Network> networkEntityToModelMapper;
    private Mapper<NodeEntity, Node> nodeEntityToModelMapper;


    @Autowired
    public NetworkService(Mapper<NodeEntity, Node> nodeEntityToModelMapper, NetworkRepository networkRepository, ChildRepository childRepository, NodeRepository nodeRepository, Mapper<Network, NetworkEntity> networkModelToEntityMapper, Mapper<NetworkEntity, Network> networkEntityToModelMapper) {
        this.networkRepository = networkRepository;
        this.childRepository = childRepository;
        this.nodeRepository = nodeRepository;
        this.networkModelToEntityMapper = networkModelToEntityMapper;
        this.networkEntityToModelMapper = networkEntityToModelMapper;
        this.nodeEntityToModelMapper = nodeEntityToModelMapper;
    }

    public Network initializeNetwork(List<Integer> sequence, String name, String description) {
        logger.info("Start network initializing");

        Network updatedNetwork = new Network(new TreeMap<>());
        updatedNetwork.setName(name);
        updatedNetwork.setDescription(description);

        for (NodeType node : NodeType.values()) {
            int amount = sequence.get(node.ordinal());
            ArrayList<Node> nodes = new ArrayList<>(amount);
            for (int i = 0; i < amount; i++) {
                nodes.add(new Node(node));
            }
            updatedNetwork.getTree().put(node, nodes);
        }

        logger.info("Network was initialized");
        return updatedNetwork;
    }

    public void createNetwork(List<Integer> sequence, String name, String description) {

        logger.info("Start network creating");

        networkRepository.save(networkModelToEntityMapper.transform(initializeNetwork(sequence, name, description)));

        logger.info("Network was created");
    }

    @Override
    public Network findNetwork(long id) {
        logger.info("Start finding network by id");

        return networkRepository.findById(id).map(networkEntityToModelMapper::transform).orElseThrow(() -> new NetworkDoesntExistException("Network doesn't exist!"));
    }

    @Override
    public Network findNetwork(String name) {

        logger.info("Start finding network by name");

        return networkRepository.findByName(name).map(networkEntityToModelMapper::transform).orElseThrow(() -> new NetworkDoesntExistException("Network doesn't exist!"));

    }

    @Override
    @Transactional
    public void deleteNetwork(String name) {

        logger.info("Start deleting network by name");

        networkRepository.deleteByName(name);

        logger.info("Network was deleted");

    }


    @Override
    @Transactional
    public void updateNetwork(Long id, List<Integer> sequence, String name, String description) {

        logger.info("Start network updating");


        NetworkEntity networkEntity = networkModelToEntityMapper.transform(initializeNetwork(sequence, name, description));
        networkEntity.setId(id);

        networkRepository.save(networkEntity);

        logger.info("Network was updated");

    }

    @Override
    public Node findNodeEntityByTypeAndName(NodeType nodeType, String name) {

        logger.info("Start finding node by type and name");

        return nodeEntityToModelMapper.transform(nodeRepository.findNodeEntityByTypeAndName(nodeType, name));
    }

    @Override
    public void updateNode(Long id, String name, String description, Map<String, String> params) {

        logger.info("Start node updating");

        NodeEntity nodeEntity = nodeRepository.findById(id).orElseThrow(() -> new NodeDoesntExistException("Node doesn't exist"));

        nodeEntity.setName(name);
        nodeEntity.setDescription(description);
        nodeEntity.setParameters(params);

        nodeRepository.save(nodeEntity);

        logger.info("Node was updated");

    }

    @Override
    public Network findNetworkByChildrenTypeAndName(NodeType nodeType, String name) {

        logger.info("Start finding network by child type and name");


        return networkEntityToModelMapper.transform(
                networkRepository.findNetworkEntityByChildContains(
                        childRepository.findChildEntityByNodesContains(
                                nodeRepository.findNodeEntityByTypeAndName(nodeType, name))));
    }

    @Override
    public void addChildToSpecificNode(NodeType nodeType, String nodeName, String newNodeName, String newNodeDescription, Map<String, String> newNodeParams) {

        logger.info("Start creating child for specific node");

        if (nodeType.equals(NodeType.RESOURCE)) {
            throw new CreationNodeException("Can't create child for this type of node!");
        }

        NetworkEntity networkEntity = networkRepository.findNetworkEntityByChildContains(
                childRepository.findChildEntityByNodesContains(
                        nodeRepository.findNodeEntityByTypeAndName(nodeType, nodeName)));

        List<ChildEntity> list = networkEntity.getChild();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getNodesType().equals(nodeType)) {

                List<NodeEntity> nodes = list.get(i).getNodes();

                for (NodeEntity node : nodes) {
                    if (node.getName() != null && node.getName().equals(nodeName)) {

                        ChildEntity buff = list.get(++i);

                        NodeEntity nodeEntity = new NodeEntity();
                        nodeEntity.setType(buff.getNodesType());
                        nodeEntity.setName(newNodeName);
                        nodeEntity.setDescription(newNodeDescription);
                        nodeEntity.setParameters(newNodeParams);
                        nodeEntity.setChild(buff);

                        nodeRepository.save(nodeEntity);

                        logger.info("Child was created");

                    }
                }
            }
        }
    }

    @Override
    public void deleteSpecificNodeChild(NodeType nodeType, String nodeName, String target) {

        logger.info("Start deleting child by name");


        if (nodeType.equals(NodeType.RESOURCE)) {
            throw new CreationNodeException("Can't create child for this type of node!");
        }

        NetworkEntity networkEntity = networkRepository.findNetworkEntityByChildContains(
                childRepository.findChildEntityByNodesContains(
                        nodeRepository.findNodeEntityByTypeAndName(nodeType, nodeName)));

        List<ChildEntity> list = networkEntity.getChild();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getNodesType().equals(nodeType)) {

                List<NodeEntity> nodes = list.get(i).getNodes();

                for (NodeEntity node : nodes) {
                    if (node.getName() != null && node.getName().equals(nodeName)) {
                        ChildEntity buff = list.get(++i);
                        for (NodeEntity deletedNode : buff.getNodes()) {
                            if (deletedNode.getName().equals(target))
                                nodeRepository.delete(deletedNode);
                        }

                        logger.info("Child was deleted");
                    }
                }
            }
        }
    }
}
