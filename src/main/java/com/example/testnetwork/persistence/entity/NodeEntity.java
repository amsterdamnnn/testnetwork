package com.example.testnetwork.persistence.entity;

import com.example.testnetwork.enums.NodeType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Entity(name = "nodes")
public class NodeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(value = EnumType.STRING)
    @NotNull
    @Column(columnDefinition = "varchar(255) default RESOURCE'")
    private NodeType type;

    @Column(unique=true)
    private String name;

    private String description;

    @ElementCollection
    @CollectionTable(name = "param_map",
            joinColumns = {@JoinColumn(name = "node_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "param_name")
    @Column(name = "value")
    private Map<String, String> parameters;

    @ManyToOne
    @JoinColumn(name="network_child_id")
    private ChildEntity child;

    public NodeEntity() {
    }

    public long getId() {
        return id;
    }

    public NodeType getType() {
        return type;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public ChildEntity getChild() {
        return child;
    }

    public void setChild(ChildEntity child) {
        this.child = child;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }


}
