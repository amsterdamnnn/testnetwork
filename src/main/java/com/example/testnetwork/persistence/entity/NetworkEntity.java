package com.example.testnetwork.persistence.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity(name = "networks")
public class NetworkEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique=true)
    private String name;

    private String description;

    @ElementCollection
    @CollectionTable(name = "network_param_map",
            joinColumns = {@JoinColumn(name = "network_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "network_param_name")
    @Column(name = "value")
    private Map<String, String> parameters;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "network")
    private List<ChildEntity> child;

    public NetworkEntity() {
    }

    public long getId() {
        return id;
    }

    public List<ChildEntity> getChild() {
        return child;
    }

    public void setChild(List<ChildEntity> child) {
        this.child = child;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public void setId(long id) {
        this.id = id;
    }
}
