package com.example.testnetwork.persistence.entity;

import com.example.testnetwork.enums.NodeType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity(name = "children")
public class ChildEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(value = EnumType.STRING)
    @NotNull
    private NodeType nodesType;

    @ManyToOne
    @JoinColumn(name="network_id")
    private NetworkEntity network;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "child")
    private List<NodeEntity> nodes;

    public ChildEntity() {
    }

    public long getId() {
        return id;
    }

    public NodeType getNodesType() {
        return nodesType;
    }

    public void setNodesType(NodeType nodesType) {
        this.nodesType = nodesType;
    }

    public NetworkEntity getNetwork() {
        return network;
    }

    public void setNetwork(NetworkEntity network) {
        this.network = network;
    }

    public List<NodeEntity> getNodes() {
        return nodes;
    }

    public void setNodes(List<NodeEntity> nodes) {
        this.nodes = nodes;
    }


}
