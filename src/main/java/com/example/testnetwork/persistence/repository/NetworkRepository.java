package com.example.testnetwork.persistence.repository;

import com.example.testnetwork.persistence.entity.ChildEntity;
import com.example.testnetwork.persistence.entity.NetworkEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NetworkRepository extends CrudRepository<NetworkEntity, Long> {

    Optional<NetworkEntity> findByName(String name);

    void deleteByName(String name);

    NetworkEntity findNetworkEntityByChildContains(ChildEntity childEntity);


}