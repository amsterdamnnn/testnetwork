package com.example.testnetwork.persistence.repository;

import com.example.testnetwork.enums.NodeType;
import com.example.testnetwork.persistence.entity.NodeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeRepository extends CrudRepository<NodeEntity, Long> {

    NodeEntity findNodeEntityByTypeAndName(NodeType nodeType, String name);
}
