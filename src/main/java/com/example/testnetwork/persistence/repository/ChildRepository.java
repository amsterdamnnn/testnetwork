package com.example.testnetwork.persistence.repository;

import com.example.testnetwork.persistence.entity.ChildEntity;
import com.example.testnetwork.persistence.entity.NodeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildRepository extends CrudRepository<ChildEntity, Long> {

    ChildEntity findChildEntityByNodesContains(NodeEntity nodeEntity);
}
