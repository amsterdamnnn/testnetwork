package com.example.testnetwork.mappers;

import com.example.testnetwork.model.Network;
import com.example.testnetwork.model.Node;
import com.example.testnetwork.persistence.entity.NetworkEntity;
import com.example.testnetwork.persistence.entity.NodeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Component
public class NetworkEntityToModelMapper implements Mapper<NetworkEntity, Network> {

    private Mapper<NodeEntity, Node> nodeEntityToModelMapper;

    @Autowired
    public NetworkEntityToModelMapper(Mapper<NodeEntity, Node> nodeEntityToModelMapper) {
        this.nodeEntityToModelMapper = nodeEntityToModelMapper;
    }

    @Override
    public Network transform(NetworkEntity from) {
        Network network = new Network(new TreeMap<>());
        network.setName(from.getName());
        network.setDescription(from.getDescription());
        network.setParameters(from.getParameters());

        for(int i = 0; i < from.getChild().size(); i++ ){
            network.getTree().put(from.getChild().get(i).getNodesType(),
                    (ArrayList<Node>) from.getChild().get(i).getNodes().stream().map(nodeEntityToModelMapper::transform).collect(Collectors.toList()));
        }

        return network;
    }
}
