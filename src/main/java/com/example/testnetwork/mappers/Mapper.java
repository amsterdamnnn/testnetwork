package com.example.testnetwork.mappers;

public interface Mapper<F,T> {
    T transform(F from);
}
