package com.example.testnetwork.mappers;

import com.example.testnetwork.model.Node;
import com.example.testnetwork.persistence.entity.NodeEntity;
import org.springframework.stereotype.Component;

@Component
public class NodeEntityToModelMapper implements Mapper<NodeEntity, Node> {

    @Override
    public Node transform(NodeEntity from) {
        Node node = new Node();
        node.setId(from.getId());
        node.setName(from.getName());
        node.setDescription(from.getDescription());
        node.setType(from.getType());
        node.setParameters(from.getParameters());
        return node;
    }
}
