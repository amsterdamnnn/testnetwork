package com.example.testnetwork.mappers;

import com.example.testnetwork.model.Node;
import com.example.testnetwork.persistence.entity.NodeEntity;
import org.springframework.stereotype.Component;

@Component
public class NodeModelToEntityMapper implements Mapper<Node, NodeEntity>{

    @Override
    public NodeEntity transform(Node from) {
        NodeEntity nodeEntity = new NodeEntity();
        nodeEntity.setName(from.getName());
        nodeEntity.setDescription(from.getDescription());
        nodeEntity.setType(from.getType());
        nodeEntity.setParameters(from.getParameters());
        return nodeEntity;
    }
}
