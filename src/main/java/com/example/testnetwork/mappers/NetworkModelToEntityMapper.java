package com.example.testnetwork.mappers;

import com.example.testnetwork.enums.NodeType;
import com.example.testnetwork.model.Network;
import com.example.testnetwork.persistence.entity.ChildEntity;
import com.example.testnetwork.persistence.entity.NetworkEntity;
import com.example.testnetwork.persistence.entity.NodeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class NetworkModelToEntityMapper implements Mapper<Network, NetworkEntity>{

    private NodeModelToEntityMapper nodeModelToEntityMapper;

    @Autowired
    public NetworkModelToEntityMapper(NodeModelToEntityMapper nodeModelToEntityMapper) {
        this.nodeModelToEntityMapper = nodeModelToEntityMapper;
    }

    @Override
    public NetworkEntity transform(Network from) {

        NetworkEntity networkEntity = new NetworkEntity();
        networkEntity.setName(from.getName());
        networkEntity.setDescription(from.getDescription());

        List<ChildEntity> childEntityList = new ArrayList<>(from.getTree().keySet().size());

        for (NodeType node : NodeType.values()) {
            ChildEntity childEntity = new ChildEntity();
            childEntity.setNetwork(networkEntity);
            childEntity.setNodesType(node);
            childEntity.setNodes((from.getTree().get(node).stream().map(nodeModelToEntityMapper::transform).collect(Collectors.toList())));
            for (NodeEntity nodeEntity : childEntity.getNodes()) {
                nodeEntity.setChild(childEntity);
            }
            childEntityList.add(childEntity);
        }
        networkEntity.setChild(childEntityList);
        return networkEntity;
    }
}
