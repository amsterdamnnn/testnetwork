package com.example.testnetwork.model;

import com.example.testnetwork.enums.NodeType;

import java.util.ArrayList;
import java.util.Map;

public class Network {

    private String name;
    private String description;
    private Map<String, String> parameters;

    private Map<NodeType, ArrayList<Node>> tree;

    public Network(Map<NodeType, ArrayList<Node>> tree) {
        this.tree = tree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Map<NodeType, ArrayList<Node>> getTree() {
        return tree;
    }

    public void setTree(Map<NodeType, ArrayList<Node>> tree) {
        this.tree = tree;
    }
}
