package com.example.testnetwork.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CreateUpdateElementRequestDto {

    private String name;
    private String description;
    private Map<String, String> parameters;

    private int amountOfSubStations;
    private int amountOfTransformers;
    private int amountOfFeeders;
    private int amountOfResources;

    List<Integer> amountOfNodesInHierarchicalOrder = new ArrayList<>();

    public List<Integer> createSequence(){
        amountOfNodesInHierarchicalOrder.add(amountOfSubStations);
        amountOfNodesInHierarchicalOrder.add(amountOfTransformers);
        amountOfNodesInHierarchicalOrder.add(amountOfFeeders);
        amountOfNodesInHierarchicalOrder.add(amountOfResources);

        return amountOfNodesInHierarchicalOrder;
    }

    public int getAmountOfSubStations() {
        return amountOfSubStations;
    }

    public void setAmountOfSubStations(int amountOfSubStations) {
        this.amountOfSubStations = amountOfSubStations;
    }

    public int getAmountOfTransformers() {
        return amountOfTransformers;
    }

    public void setAmountOfTransformers(int amountOfTransformers) {
        this.amountOfTransformers = amountOfTransformers;
    }

    public int getAmountOfFeeders() {
        return amountOfFeeders;
    }

    public void setAmountOfFeeders(int amountOfFeeders) {
        this.amountOfFeeders = amountOfFeeders;
    }

    public int getAmountOfResources() {
        return amountOfResources;
    }

    public void setAmountOfResources(int amountOfResources) {
        this.amountOfResources = amountOfResources;
    }

    public List<Integer> getAmountOfNodesInHierarchicalOrder() {
        return amountOfNodesInHierarchicalOrder;
    }

    public void setAmountOfNodesInHierarchicalOrder(List<Integer> amountOfNodesInHierarchicalOrder) {
        this.amountOfNodesInHierarchicalOrder = amountOfNodesInHierarchicalOrder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}
