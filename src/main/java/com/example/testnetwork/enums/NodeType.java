package com.example.testnetwork.enums;

public enum NodeType {

    SUBSTATION, TRANSFORMER, FEEDER, RESOURCE

}
