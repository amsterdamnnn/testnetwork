package com.example.testnetwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestNetworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestNetworkApplication.class, args);
	}
}
