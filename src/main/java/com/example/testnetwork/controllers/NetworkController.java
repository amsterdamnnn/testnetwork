package com.example.testnetwork.controllers;

import com.example.testnetwork.dto.CreateUpdateElementRequestDto;
import com.example.testnetwork.enums.NodeType;
import com.example.testnetwork.model.Network;
import com.example.testnetwork.model.Node;
import com.example.testnetwork.service.INetworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/manager")
public class NetworkController {

    INetworkService networkService;

    @Autowired
    public NetworkController(INetworkService networkService) {
        this.networkService = networkService;
    }

    @PostMapping(value = "/network/create")
    public ResponseEntity<Void> createWholeNetwork(@RequestBody CreateUpdateElementRequestDto createUpdateElementRequestDto){
        networkService.createNetwork(createUpdateElementRequestDto.createSequence(), createUpdateElementRequestDto.getName(), createUpdateElementRequestDto.getDescription());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/networks/{networkId}")
    public ResponseEntity<Network> findNetworkById(@PathVariable("networkId") Long networkId) {

        return ResponseEntity.ok(networkService.findNetwork(networkId));
    }

    @GetMapping(value = "/networks")
    public ResponseEntity<Network> findNetworkByName(@RequestParam(value="name") String networkName) {

        return ResponseEntity.ok(networkService.findNetwork(networkName));
    }

    @DeleteMapping(value = "/network/delete")
    public ResponseEntity<Void> deleteNetwork(@RequestParam(value = "name") String networkName){

        networkService.deleteNetwork(networkName);
        return ResponseEntity.accepted().build();
    }

    @PutMapping(value = "/network/update/{networkId}")
    public ResponseEntity<Network> updateNetwork(@PathVariable("networkId") Long networkId, @RequestBody CreateUpdateElementRequestDto createUpdateElementRequestDto){
        networkService.updateNetwork(networkId, createUpdateElementRequestDto.createSequence(), createUpdateElementRequestDto.getName(), createUpdateElementRequestDto.getDescription());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/node/update/{nodeId}")
    public ResponseEntity<Void> updateNode(@PathVariable("nodeId") Long nodeId, @RequestBody CreateUpdateElementRequestDto createUpdateElementRequestDto){
        networkService.updateNode(nodeId, createUpdateElementRequestDto.getName(), createUpdateElementRequestDto.getDescription(), createUpdateElementRequestDto.getParameters());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/node/find")
    public ResponseEntity<Node> findNodeByTypeAndName(@RequestParam(value = "type") NodeType nodeType, @RequestParam(value = "name") String nodeName){

        return ResponseEntity.ok(networkService.findNodeEntityByTypeAndName(nodeType, nodeName));
    }

    @GetMapping(value = "/network")
    public ResponseEntity<Network> findNetworkByChildrenTypeAndName(@RequestParam(value = "type") NodeType nodeType, @RequestParam(value = "name") String nodeName) {

        return ResponseEntity.ok(networkService.findNetworkByChildrenTypeAndName(nodeType, nodeName));
    }

    @PostMapping(value = "/network/addChild")
    public ResponseEntity<Void> addChildToSpecificNode(@RequestParam(value = "type") NodeType nodeType, @RequestParam(value = "name") String nodeName, @RequestBody CreateUpdateElementRequestDto createUpdateElementRequestDto){
        networkService.addChildToSpecificNode(nodeType, nodeName, createUpdateElementRequestDto.getName(), createUpdateElementRequestDto.getDescription(), createUpdateElementRequestDto.getParameters());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "network/deleteChild/{target}")
    public ResponseEntity<Void> deleteSpecificNodeChild(@PathVariable("target") String target, @RequestParam(value = "type") NodeType nodeType, @RequestParam(value = "name") String nodeName){

        networkService.deleteSpecificNodeChild(nodeType, nodeName, target);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
