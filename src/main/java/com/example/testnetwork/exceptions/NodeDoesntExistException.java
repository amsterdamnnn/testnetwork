package com.example.testnetwork.exceptions;

public class NodeDoesntExistException extends RuntimeException {

    public NodeDoesntExistException() {
    }

    public NodeDoesntExistException(String message) {
        super(message);
    }
}