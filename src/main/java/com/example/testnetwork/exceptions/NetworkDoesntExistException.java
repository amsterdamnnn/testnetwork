package com.example.testnetwork.exceptions;

public class NetworkDoesntExistException extends RuntimeException {

    public NetworkDoesntExistException() {
    }

    public NetworkDoesntExistException(String message) {
        super(message);
    }
}
