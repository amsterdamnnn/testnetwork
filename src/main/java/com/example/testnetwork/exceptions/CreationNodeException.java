package com.example.testnetwork.exceptions;

public class CreationNodeException extends RuntimeException {

    public CreationNodeException() {
    }

    public CreationNodeException(String message) {
        super(message);
    }
}
